# Dockercoins Charts
Helm charts for Jérôme Petazzo's Dockercoins on Kubernetes (locally) Coding Challenge

## Generated from Helm
```shell
╰─$ helm version
version.BuildInfo{Version:"v3.6.1", GitCommit:"61d8e8c4a6f95540c15c6a65f36a6dd0a45e7a2f", GitTreeState:"dirty", GoVersion:"go1.16.5"}
╰─$ helm create dockercoins-charts
╰─$ kubectl create --filename=namespace.yaml
namespace/dockercoins created
```
Set the default namespace in `~.kube/config` for `minikube` to `dockercoins`

# Deploy the Application with Helm
```shell
% helm install dockercoins .
% helm list
% kubectl get all --output=wide
```

# Uninstall the Application with Helm
```shell
% helm uninstall dockercoins .
% helm list
% kubectl get all --output=wide
```

# Get a URL for the Web UI
```shell
% minikube service dockercoins-webui --namespace dockercoins --url
```



